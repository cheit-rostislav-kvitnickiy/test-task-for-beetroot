var dishes = [
    {
        name: "Овсяная каша с фруктами",
        cost: 25,
        img: 'i/im1.jpg',
        cat: 1
    },
    {
        name: "Яичница глазунья с овощами на сковородке",
        cost: 25,
        img: 'i/im2.jpg',
        cat: 3
    },
    {
        name: "Сет азербайджанский завтрак",
        cost: 30,
        img: 'i/im3.jpg',
        cat: 1
    },
    {
        name: "Яичница с помидорами по-бакински",
        cost: 45,
        img: 'i/im4.jpg',
        cat: 2
    },
    {
        name: "Сырники со сметаной",
        cost: 45,
        img: 'i/im5.jpg',
        cat: 1
    },
    {
        name: "Шпинатный крем-суп",
        cost: 50,
        img: 'i/im6.jpg',
        cat: 1
    },
    {
        name: "Суп Пити",
        cost: 85,
        img: 'i/im7.jpg',
        cat: 2
    },
    {
        name: "Борщ украинский",
        cost: 95,
        img: 'i/im8.jpg',
        cat: 1
    },
    {
        name: "Суп Кюфта Бозбаш",
        cost: 100,
        img: 'i/im9.jpg',
        cat: 3
    },
    {
        name: "Картофель фри",
        cost: 125,
        img: 'i/im10.jpg',
        cat: 1
    },
    {
        name: "Картофель по-домашнему",
        cost: 135,
        img: 'i/im11.jpg',
        cat: 2
    },
    {
        name: "Рис с овощами",
        cost: 150,
        img: 'i/im12.jpg',
        cat: 3
    }
];

function FoodMenu( dishes ) {
    this.total = 0;
    this.count = 0;
    this.dishes = dishes;

    this.showItems = function(items) {
        var dish = '',
            dishesBlock = document.querySelector('.products-box_fn');

        items.forEach( function (item) {
            dish = document.querySelector('.products-box_example .product-box__item').cloneNode(true);

            dish.querySelector('.product-box__title').innerHTML = item.name;
            dish.querySelector('.product-box__cost').innerHTML = item.cost;
            dish.querySelector('.product-box__img img').setAttribute( "src", item.img);

            dishesBlock.appendChild( dish );
        });
    };

    this.removeItems = function() {
        document.querySelector('.products-box_fn').innerHTML = '';
    }

    this.showAllDishes = function() {
        this.showItems( this.dishes );
    }

    this.updatePrice = function(price) {
        this.total += parseInt( price );
        document.querySelector('.red-info_price').innerHTML = this.total;

        console.log( 'price: ' + this.total );
    }

    this.updateCount = function(count) {
        this.count += parseInt( count );
        document.querySelector('.red-info_count').innerHTML = this.count;

        console.log( 'count' + this.count );
    }

    this.filter = function(cat, price) {
        var localDishes = [];

        this.dishes.forEach( function (item) {
            if( item.cat == cat && item.cost < price || item.cat == cat && 0 == price || 0 == cat && item.cost < price ){
                localDishes.push( item );
            }
        });

        localDishes = cat == 0 &&  price == 0 ? this.dishes : localDishes;

        this.removeItems();
        this.showItems( localDishes );
    }

    this.initActions = function() {
        var self = this;

        document.querySelector('.products-box_fn').addEventListener("click", function(event){
            if( !event.target.closest('.product-box__btn') ) return;

            var dataBlock = event.target.parentNode;
            var actionCount = dataBlock.querySelector('.qty__item').value,
                dishCost = dataBlock.querySelector('.product-box__cost').innerHTML;

            if( actionCount > 0 ){
                self.updatePrice( dishCost * actionCount );
                self.updateCount( actionCount );
            }
        }, false);

        document.querySelector('.select-box .select-control').addEventListener("change", function(event){
            var cat = this.value,
                price = document.querySelector('.price-select-box .select-control').value;

            self.filter(cat, price);
        });

        document.querySelector('.price-select-box .select-control').addEventListener("change", function(event){
            var price = this.value,
                cat = document.querySelector('.select-box .select-control').value;

            self.filter(cat, price);
        });
    }

    this.initForm = function() {

        var modal = document.getElementById('order-modal');
        var btn = document.getElementById('order-btn');
        var span = document.querySelector('.close');

        btn.onclick = function(){
            modal.style.display = "block";
        };
        span.onclick = function(){
            modal.style.display = "none";
        };
        window.onclick = function(){
            if(event.target == modal){
                modal.style.display = "none";
            }
        };

        var formBtn = document.getElementById('form__btn'),
            form = formBtn.parentElement,
            nameInput = form.querySelector(".form__name"),
            emailInput = form.querySelector(".form__e-mail"),
            formError = form.querySelector(".form__error"),
            thankBlock = document.getElementById('thank'),
            pattern = /^[\s]+$/ ;

        formBtn.onclick = function () {
            var nameInputVal = nameInput.value,
                emailInputVal = emailInput.value;
            if (nameInputVal == "" || emailInputVal == "" || pattern.test(nameInputVal) || pattern.test(emailInputVal)) {
                formError.style.display = "block";
            } else {
                counterOfItems = 0;
                counterOfPrice = 0;
                document.querySelector('.red-info_count').innerHTML = 'XXX';
                document.querySelector('.red-info_price').innerHTML = 'XXX';
                modal.style.display = "none";
                thankBlock.style.display = "block";
                setTimeout(function(){
                    thankBlock.style.display = "none";
                },1600)
            }
        };
    }

};

var menu = new FoodMenu( dishes );
    menu.showAllDishes();
    menu.initActions();
    menu.initForm();